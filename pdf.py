import PyPDF2
# import sys



# 1. Citire si scriere(rotire) pdf

# with open('dummy.pdf', 'rb') as file: # trebuie sa punem rb - read binary, altfel nu va citi fisierul - da eroare. Se creaza un obiect binar(0,1)
#     # print(file)   # pentru a vedea daca se deschide fisierul pentru citire.
#     reader = PyPDF2.PdfFileReader(file) # Aici cu PyPDF2 se citeste fisierul binar
#     # print(reader.numPages) # se afiseaza numarul de pagini => 1
#     # print(reader.getPage(0)) # se afiseaza prima pagina(obiectul special creeat de PYPDF2, daca punem 1 da eroare pentru ca avem doar o pagina,
#     page = reader.getPage(0) # trebuie sa creem obiectul pagina cand vrem sa intervenim asupra paginii respective
#     page.rotateCounterClockwise(90) # aici se salveaza obiectul nou creat in memoria calculatorului dar nu se modifica nimic pe pagina. Actiune de rotire a paginii!!!
#
#     # Update dupa citire si scriere conform functiei print() de mai sus.
#
#     writer = PyPDF2.PdfFileWriter() # aici deschidem obiectul writer pt scriere dar nu scriem nimic in el
#     writer.addPage(page) # aici ii dam lui writer pagina pe care o rotim (page.rotateCounterClockwise(90))
#     with open('tilt.pdf', 'wb') as new_file:
#         writer.write(new_file)


#2. Unirea a mai multor pdf-uri

# inputs = sys.argv[1:] # aici vom aduce toate file-urile noastre intr-o lista, fara limita superioara.
#
# def pdf_combiner(pdf_list): # se poate folosi cand avem o carte la care lucreaza mai multe persoane pentru a combina la final pdf-urile
#     merger = PyPDF2.PdfFileMerger() # aici obtinem merger object
#     for pdf in pdf_list:
#         print(pdf) # verificare daca apar pdf-urile
#         merger.append(pdf) #adaugam pdf-urile la obiectul nou creat
#     merger.write('super.pdf') # si le scriem cu functia write() adaugand si nume obiectului ,merger'
#
# pdf_combiner(inputs)

# 3. Watermarker the PDF file

template = PyPDF2.PdfFileReader(open('super.pdf', 'rb')) # aici creem file-ul reader in memorie calc si deschidem fisierul cu acest shortcut.
watermark = PyPDF2.PdfFileReader(open('wtr.pdf', 'rb'))
ouput = PyPDF2.PdfFileWriter() #aici creem file-ul writer in memorie calc

for i in range(template.getNumPages()): # atentie aici functia getNumpages() ne spune cate pagini vom avea pentru ca nu stim cate vor fi
    page = template.getPage(i) # creem obiectul page care va contine toate paginile primite de la for bucla
    page.mergePage(watermark.getPage(0)) # ,0' pentru ca watermark page are doar o pagina
    ouput.addPage(page) # adaugam pagina margiuta la obiectul ouput creart mai sus pentru scriere in el

    with open('watermarked_output.pdf', 'wb') as file:
        ouput.write(file)








